Report

Project: Twins of The Main Manchester United Club Website

Member: 1.Taywin Phetchaboon 6110210568 2.Jirayud Kasemsuk 6110210055

Problem: The main Manchester United website is used around the world. 
There is no Thai language in the website's additional language change function. 
Therefore causing problems for Thai football fans who want to use certain websites, such as taking a long time to understand or making mistakes. 
This system will help facilitate Thai football fans who want to use the website and face this problem.

Proposal: In this project, we propose Manchester United club information, shop online, etc. 
It will have the similar information as the main website.  
To help facilitate Thai football fans who want to use the Manchester United club website  Therefore, Thai football fans will get quicker and easier to perform various tasks compared to using the main website.

Technologies Used: 
- dotnet core 2.1

List of Features (Prototype): 
- Frontend for displaying Update News


List of Features (Complete Version): 
- Option order
- Membership signing up
- Online ticket sales system 
- New news notification