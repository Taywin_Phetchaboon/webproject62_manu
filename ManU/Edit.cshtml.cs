using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace ManU
{
    public class EditModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public EditModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsCategory NewsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsCategory = await _context.NewsCategory.FirstOrDefaultAsync(m => m.NewsCategoryID == id);

            if (NewsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(NewsCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsCategoryExists(NewsCategory.NewsCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewsCategoryExists(int id)
        {
            return _context.NewsCategory.Any(e => e.NewsCategoryID == id);
        }
    }
}
