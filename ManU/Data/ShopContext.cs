using Shop.Models;
using Microsoft.EntityFrameworkCore;

namespace Shop.Data
{
    public class ShopContext : DbContext
    {
        public DbSet<Album> Album { get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Album.db");
        }
    }
}