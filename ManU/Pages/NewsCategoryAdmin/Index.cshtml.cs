using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace ManU.Pages.NewsCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public IndexModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public IList<NewsCategory> NewsCategory {get; set;}

        public async Task OnGetAsync()
        {
            NewsCategory = await _context.NewsCategory.ToListAsync();
        }
    }
}
