using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

using Shop.Models;
using Shop.Data;

namespace ManU.Pages
{
    public class ShopModel : PageModel
    {
        private readonly Shop.Data.ShopContext _context;

        public ShopModel(Shop.Data.ShopContext context)
        {
            _context = context;
        }

        public IList<Album> Album { get;set; }

        public async Task OnGetAsync()
        {
            Album = await _context.Album.ToListAsync();
        }
    }
}