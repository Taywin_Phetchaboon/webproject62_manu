using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace ManU
{
    public class DeleteModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public DeleteModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsCategory NewsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsCategory = await _context.NewsCategory.FirstOrDefaultAsync(m => m.NewsCategoryID == id);

            if (NewsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsCategory = await _context.NewsCategory.FindAsync(id);

            if (NewsCategory != null)
            {
                _context.NewsCategory.Remove(NewsCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
