﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ManU.Migrations
{
    public partial class Win : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsCategory",
                columns: table => new
                {
                    NewsCategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCategory", x => x.NewsCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "newsList",
                columns: table => new
                {
                    NewsID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NewsCategoryID = table.Column<int>(nullable: false),
                    ReportData = table.Column<string>(nullable: true),
                    NewsDetail = table.Column<string>(nullable: true),
                    GpsLat = table.Column<float>(nullable: false),
                    GpsLng = table.Column<float>(nullable: false),
                    NewsStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_newsList", x => x.NewsID);
                    table.ForeignKey(
                        name: "FK_newsList_NewsCategory_NewsCategoryID",
                        column: x => x.NewsCategoryID,
                        principalTable: "NewsCategory",
                        principalColumn: "NewsCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_newsList_NewsCategoryID",
                table: "newsList",
                column: "NewsCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "newsList");

            migrationBuilder.DropTable(
                name: "NewsCategory");
        }
    }
}
