﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Shop.Data;

namespace ManU.Migrations.Shop
{
    [DbContext(typeof(ShopContext))]
    partial class ShopContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024");

            modelBuilder.Entity("Shop.Models.Album", b =>
                {
                    b.Property<int>("AlbumID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AlbumImage");

                    b.Property<string>("AlbumName");

                    b.Property<string>("ArtistName");

                    b.Property<double>("Price");

                    b.Property<DateTime>("ReleaseDate");

                    b.Property<string>("SampleAudio");

                    b.Property<int>("stock");

                    b.HasKey("AlbumID");

                    b.ToTable("Album");
                });
#pragma warning restore 612, 618
        }
    }
}
