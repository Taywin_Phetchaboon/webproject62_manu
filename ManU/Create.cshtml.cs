using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NewsReport.Data;
using NewsReport.Models;

namespace ManU
{
    public class CreateModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public CreateModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public NewsCategory NewsCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.NewsCategory.Add(NewsCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}